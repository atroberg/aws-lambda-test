module.exports = {
  entry: './functions/helloWorld.js',
  target: 'node',
  module: {
    loaders: [
      {
        loader: 'babel',
        plugins: [
          'transform-object-rest-spread',
          'transform-class-properties',
          'transform-es2015-modules-commonjs'
        ]
      }
    ]
  },
  output: {
    libraryTarget: 'commonjs',
    path: '.webpack',
    filename: 'functions/helloWorld.js'
  }
}
