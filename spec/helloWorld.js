import test from 'tape'
import { hello } from '../functions/helloWorld'

test('Hello world', t => {
  t.plan(1)

  hello(null, null, (error, response) => {
    t.equal(response.message, 'Hello')
  })
})
