'use strict';

const defaultProps = {
  a: 'test',
  b: 'test2'
}

export const hello = (event, context, cb) => {
  cb(null, {
    message: 'Hello',
    event,
    ...defaultProps
  })
}
